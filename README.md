# Website

Just a simple landing page with contact info so people can find me again.

### TODO

- [ ] Replace slide show images with proper ones.
- [ ] Replace profile picture (or at least make it square).
- [ ] Tweak colors + fonts.
- [ ] Make it responsive.
